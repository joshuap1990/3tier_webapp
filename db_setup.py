import sqlite3

# Create a new SQLite database (or connect to an existing one)
conn = sqlite3.connect("my_database.db")

# Create a cursor object to execute SQL commands
cursor = conn.cursor()



# Define a schema for an "LOA TYPE" table
cursor.execute('''
    CREATE TABLE IF NOT EXISTS LOA_TYPE (
        LOA_TYPE_ID INTEGER PRIMARY KEY AUTOINCREMENT,
        LOA_TYPE STRING NOT NULL,
        TRANSACTION_TYPE STRING NOT NULL
    )
''')

# Define a schema for an "LOA_STATUS" table
cursor.execute('''
    CREATE TABLE IF NOT EXISTS LOA_STATUS (
        LOA_STATUS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
        LOA_STATUS STRING NOT NULL
    )
''')

# Define a schema for an "LOA" table
cursor.execute('''
    CREATE TABLE IF NOT EXISTS LOA (
        loa_id INTEGER PRIMARY KEY AUTOINCREMENT,
        loa_type TEXT,
        loa_date TEXT,
        sett_date TEXT,
        loa_amt TEXT,
        modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP

    )
''')


# Commit the changes and close the database connection
conn.commit()
conn.close()
