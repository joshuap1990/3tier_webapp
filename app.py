from flask import Flask, render_template, request, jsonify
import sqlite3

app = Flask(__name__)



@app.route('/')
def home():
    return render_template('index.html')

@app.route('/create')
def create_record():
    return render_template('create.html')

@app.route('/view')
def view_records():
    return render_template('view.html')

@app.route('/api/items', methods=['GET', 'POST'])
def api_items():
    if request.method == 'GET':
        conn = sqlite3.connect("my_database.db")
        cursor = conn.cursor()
        cursor.execute("SELECT loa_id, loa_type, loa_date, sett_date,loa_amt FROM LOA")
        items = cursor.fetchall()
        conn.close()

        item_list = [{'loa_id': item[0],
                    'loa_type': item[1],
                     'loa_date': item[2],
                     'sett_date': item[3],
                     'loa_amt': item[4],
                     } for item in items]
        return jsonify(item_list)
    elif request.method == 'POST':
        data = request.get_json()
        print(data)

        
        loa_type = data['loa_type']
        loa_date = data['loa_date']
        sett_date = data['sett_date']
        loa_amt = data['loa_amt']

        conn = sqlite3.connect("my_database.db")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO LOA (loa_type, loa_date, sett_date, loa_amt) VALUES (?, ?, ?, ?)",
               (loa_type, loa_date, sett_date, loa_amt))

        last_inserted_id = cursor.lastrowid

        conn.commit()
        conn.close()

        return jsonify({'message': f'LOA Number {last_inserted_id} successfully generated'}), 201

@app.route('/api/update', methods=['GET','POST'])
def api_update():
    if request.method =='GET':
        pass

    elif request.method =='POST':
        pass

if __name__ == '__main__':
    app.run(debug=True)
