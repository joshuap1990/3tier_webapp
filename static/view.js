
// JavaScript code to fetch and display records from the API
function fetchRecords() {
    fetch('/api/items')
    .then(response => response.json())
    .then(data => {
        const recordList = document.getElementById('record-list');
        recordList.innerHTML = '';
        data.forEach(record => {
            const tr = document.createElement('tr');
            tr.setAttribute("id","row_" + record.loa_id);
            const loa_id = record.loa_id;
            tr.innerHTML = `
                <td id="loa_id_${record.loa_id}"> ${record.loa_id}</td>
                <td> <input id="loa_type_${record.loa_id}" type="text" value="${record.loa_type}"></td>
                <td> <input id="loa_date_${record.loa_id}" type="text" value="${record.loa_date}"></td>
                <td> <input id="sett_date_${record.loa_id}" type="text" value="${record.sett_date}"></td>
                <td> <input id="loa_amt_${record.loa_id}" type="text" value="${record.loa_amt}"></td>
                <td>
                    <button onclick="updateRecord(${loa_id})">Update</button>
                    <button onclick="printToPDF(${loa_id})">Print to PDF</button>
                </td>
            `;
            recordList.appendChild(tr);
        });
    });
}

// Function to update the record
function updateRecord(loa_id) {
    
    
    const dynamicid = "loa_type_" + loa_id;
    console.log(dynamicid)

    const selected = document.getElementById(dynamicid)
    console.log(selected)
    const val = selected.value
    console.log(val)

    const updatedData = {
        id: loa_id,
        loa_type: document.getElementById('loa_type_' + loa_id).value,
        loa_date: document.getElementById('loa_date_' + loa_id).value,
        sett_date: document.getElementById('sett_date_' + loa_id).value,
        loa_amt: document.getElementById('loa_amt_' + loa_id).value,
    };
    console.log(updatedData)
    
    // Send the updated data to the backend via an HTTP request
    fetch('/api/update', {
        method: 'PUT', // or 'POST' depending on your API
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedData)
    })
    .then(response => {
        if (response.ok) {
            // Update successful, you may want to provide feedback to the user
            fetchRecords(); // Refresh the data to reflect the changes
        } else {
            // Handle errors, show an error message, etc.
            console.error('Update failed');
        }
    })
    .catch(error => {
        console.error('Update error:', error);
    });
}

// Function to print the record to PDF
function printToPDF(loa_id) {
    // Implement logic to generate and print the PDF for the record
    // You may use a JavaScript PDF library like pdfmake or jsPDF for this
    // Ensure the generated PDF is opened for the user to download or view
}

// Fetch and display records when the page loads
fetchRecords();
